<?php

/**
 * Administration form for git manager
 */
function gitmanager_admin_form($form_state) {
  $form = array();
  $form['gitmanager_repositories_location'] = array(
    '#title' => 'Repositories location',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('gitmanager_repositories_location', '/git'),
    '#description' => t('Provide a valid URL location e.g. /var/lib/git-repos or user@host:/var/lib/git-repos'),
  );

  $default_value = explode(' ', 'root daemon bin sys sync games man lp mail news uucp proxy www-data backup list irc gnats nobody libuuid syslog mysql sshd landscape git gitmanager');
  $form['gitmanager_unix_blacklist'] = array(
    '#title' => 'Unix name blacklist',
    '#type' => 'textarea',
    '#default_value' => variable_get('gitmanager_unix_blacklist', implode(PHP_EOL, $default_value)),
    '#description' => 'List a set of users that Drupal account holders can not set their unix name too. One per line',
  );

  $form['gitmanager_authorized_keys'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('gitmanager_authorized_keys', '/gitmanager/authorized_keys'),
    '#title' => 'Authorized keys directory',
    '#description' => t("This is the location where Git Manager will write the ssh keys for users to. This should also be the location set in sshd_conf's AuthorizedKeysFile. StrictModes should also be set to 'off'"),
  );

  return system_settings_form($form);
}
