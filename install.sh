#!/bin/bash 

GIT_USER="gitmanager"
GIT_REPOS_DIR="/gitmanager"

USER_EXISTS=`cat /etc/passwd | grep $GIT_USER: | grep :$GIT_REPOS_DIR:`

set -e

if [ "$USER_EXISTS" != "" ]; then
  echo "$GIT_USER already exists."
  exit 1
fi

# Create place to store public keys of users.
if [ ! -d $GIT_REPOS_DIR/authorized_keys ]; then
  mkdir -p $GIT_REPOS_DIR/authorized_keys
fi

/usr/sbin/adduser --system \
  --disabled-login \
  --group \
  --shell /bin/false \
  --home $GIT_REPOS_DIR \
  $GIT_USER

# 
# VISUDO CONF
#
# The following configuration needs to be placed in /etc/sudoers
# # Cmnd alias specification
# Cmnd_Alias     GITCMDS = /usr/sbin/adduser, /bin/chgrp, /usr/sbin/deluser
# 
# # User privilege specification
# gitmanager ALL= NOPASSWD: GITCMDS

#
# SSHD_CONF
#
# Change these settings in /etc/ssh/sshd_config
# AuthorizedKeysFile  $GIT_REPOS_DIR/authorized_keys/%u
# StrictModes no
