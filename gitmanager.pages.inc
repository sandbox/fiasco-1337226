<?php

/**
 * View and manage user ssh-keys.
 */
function gitmanager_user_admin($user) {
  $content = array();
  if (isset($user->unix_username)) {
    $content['unix_username']['#value'] = theme('table', array(), array(
      array(array('data' => 'username', 'header' => TRUE), check_plain($user->unix_username)),
    ));

    // SSH-keys can only be added once a unix user has been created.
    $content['ssh-keys'] = array(
      '#type' => 'fieldset',
      '#title' => 'SSH Keys',
      '#collapsible' => TRUE,
      '#weight' => 1,
    );
    $rs = db_query("SELECT * FROM {gitmanager_user_keys} WHERE uid = %d", $user->uid);
    $rows = array();
    while($row = db_fetch_object($rs)) {
      $links = array(
        l('delete', 'user/' . $row->uid . '/git-access/' . $row->kid . '/delete', array('query' => 'destination=' . $_GET['q'])),
        l('edit', 'user/' . $row->uid . '/git-access/' . $row->kid . '/edit', array('query' => 'destination=' . $_GET['q'])),
      );

      $rows[] = array($row->name, implode(' ', $links));
    }
    $headers = array('SSH Key', 'Operations');
    $content['ssh-keys']['keys']['#value'] = theme('table', $headers, $rows);
    $content['ssh-keys']['form']['#value'] = drupal_get_form('gitmanager_user_edit_ssh_key', $user);
  }
  else {
    $content['unix_username']['#value'] = drupal_get_form('gitmanager_provision_unix_user_form', $user);
  }

  return drupal_render($content);
}

/**
 * Form function to add an ssh key to a user.
 */
function gitmanager_user_edit_ssh_key($form_state, $user, $kid = FALSE) {
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );
  if ($kid) {
    $record = db_fetch_object(db_query("SELECT * FROM {gitmanager_user_keys} WHERE kid = %d AND uid = %d", $kid, $user->uid));
    if (!$record) {
      unset($record);
    }
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'name',
    '#description' => 'A label to indicate where this key comes from',
    '#required' => TRUE,
    '#default_value' => isset($record) ? $record->name : ''
  );
  $form['ssh_key'] = array(
    '#type' => 'textarea',
    '#title' => 'SSH Public Key',
    '#description' => 'Paste your public rsa ssh key in here.',
    '#default_value' => isset($record) ? $record->ssh_key : '',
    '#required' => TRUE,
  );
  if (isset($record)) {
    $form['kid'] = array(
      '#type' => 'hidden',
      '#value' => $record->kid,
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Submit form function to add an ssh key to a user.
 */
function gitmanager_user_edit_ssh_key_submit($form, $form_state) {
  $record = $form_state['values'];
  if (isset($record['kid'])) {
    drupal_write_record('gitmanager_user_keys', $record, 'kid');
    drupal_set_message("SSH key updated.");
  }
  else {
    drupal_write_record('gitmanager_user_keys', $record);
    drupal_set_message("SSH key created.");
  }
  gitmanager_provision('gitmanager_update_user_keys', array($record['uid']));
}

/**
 * Form to confirm user wants to delete ssh key.
 */
function gitmanager_confirm_key_delete($form_state, $user, $kid) {
  $form = array();
  $form['kid'] = array(
    '#type' => 'hidden',
    '#value' => $kid,
  );
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );
  return confirm_form($form, "Are you sure you want to delete this key?", 'user/' . $user->uid . '/ssh-keys');
}

/**
 * Submission function for removing user ssh key.
 */
function gitmanager_confirm_key_delete_submit($form, $form_state) {
  $record = $form_state['values'];
  db_query('DELETE FROM {gitmanager_user_keys} WHERE kid = %d AND uid = %d', $record['kid'], $record['uid']);
  drupal_set_message("SSH key deleted.");  
  gitmanager_provision('gitmanager_update_user_keys', array($record['uid']));
}


/**
 * Form to provision a unix user to a server.
 */
function gitmanager_provision_unix_user_form($form_state, $account) {
  $form['unixname'] = array(
    '#title' => 'Git username',
    '#type' => 'textfield',
    '#default_value' => str_replace(' ' , '', strtolower($account->name)), 
    '#description' => t('To be able to use Git you must first have a git username. Once this is created, it cannot be changed.'),
    '#required' => TRUE,
  );
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'create',
  );
  return $form;
}

/**
 * Form validation: gitmanager_provision_unix_user_form().
 */
function gitmanager_provision_unix_user_form_validate($form, $form_state) {
  $default_value = explode(' ', 'root daemon bin sys sync games man lp mail news uucp proxy www-data backup list irc gnats nobody libuuid syslog mysql sshd landscape git gitmanager');
  if ($blacklist = variable_get('gitmanager_unix_blacklist', FALSE)) {
    $blacklist = explode("\r\n", $blacklist);
  }
  else {
    $blacklist = $default_value;
  }
  $username = trim($form_state['values']['unixname']);
  if (in_array($username, $blacklist)) {
    form_set_error('unixname', t("Cannot create user with the name '%username'. That name is reserved.", array('%username' => $username)));
  }
  $exists = db_result(db_query("SELECT COUNT(*) FROM {gitmanager_unix_users} WHERE name = '%s'", $username));
  if ($exists) {
    form_set_error('unixname', t("Sorry that username is already taken, please try another one."));
  }
}

/**
 * Form Submission: gitmanager_provision_unix_user_form().
 */
function gitmanager_provision_unix_user_form_submit($form, $form_state) {
  $record = array(
    'uid' => $form_state['values']['uid'],
    'name' => trim($form_state['values']['unixname']),
  );

  if (drupal_write_record('gitmanager_unix_users', $record)) {
    gitmanager_provision('gitmanager_create_unix_user', array(trim($form_state['values']['unixname']), $form_state['values']['uid']));
    drupal_set_message("Your account has been provisioned to be created. While you wait, add an SSK key so you can login to the server");
  }
  else {
    drupal_set_message('An error occured while trying to create your git user. Please try again later.', 'error');
  }
}
