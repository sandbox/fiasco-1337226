<?php

/**
 * Implementation of hook_githooks_event_pre_receive().
 */
function gitmanager_githooks_event_pre_receive() {
  $repository = drush_get_option('repository', FALSE);

  // If the repository isn't registered with gitmanager then don't prevent
  // the user from accessing the repository.
  if (!$rid = db_result(db_query("SELECT rid FROM {gitmanager_repositories} WHERE filepath = '%s'", $repository))) {
    return;
  }

  $unix_username = $_SERVER['USER'];

  // Check that the user's Unix account is associated with a Drupal account.
  if (!$uid = db_result(db_query("SELECT uid FROM {gitmanager_unix_users} WHERE name = '%s'", $unix_username))) {
    return drush_set_error(__FUNCTION__, "You are not registered to use Git VCS on this server.");
  }

  $account = user_load(array('uid' => $uid));

  // Check that the user is allows to use gitmanager.
  if (!user_access('use gitmanager', $account)) {
    return drush_set_error(__FUNCTION__, "You are not allowed to use repositories managed by gitmanager.");
  }

  // Check the user's account is allows to access this repo.
  if (!gitmanager_repo_access($rid, $account)) {
    return drush_set_error(__FUNCTION__, "You do not have the access permissions to write to this repository");
  }
}

/**
 * Execute a shell command.
 */
function gitmanager_execute($command) {
  $args = func_get_args();
  array_shift($args);
  array_unshift($args, gitmanager_prepare_command($command));
  return call_user_func('_drush_shell_exec', $args);
}

/**
 * Prepare a shell command.
 */
function gitmanager_prepare_command($command) {
  $url = parse_url(variable_get('gitmanager_repositories_location', 'gitmanager@localhost:/git'));
  $subs = array('%path' => realpath($url['path']));
  if (isset($url['user'])) {
    $subs['%user'] = $url['user']; 
  }
  if (isset($url['host'])) {
    $subs['%host'] = $url['host'];
  }
  if (isset($url['host'], $url['user']) && $url['host'] != "localhost") {
    $command = 'ssh %user@%host ' . $command;
  }
  return strtr($command, $subs);
}

/**
 * Create a git repository.
 */
function gitmanager_create_repository($repository) {
  $filepath = gitmanager_prepare_command('%path/' . $repository->filepath . '.git');
  if (!gitmanager_execute(sprintf('if [ -d %s ]; then exit 1; fi', $filepath))) {
    watchdog('gitmanager', "Cannot create repository %name. Repository already exists", array('%name' => $repository->name), WATCHDOG_ERROR);
    return FALSE;
  }

  // Create the repository and make it writeable by a group.
  // Users who can write to this repo will become members of 
  // this group.
  gitmanager_execute("sudo adduser --group %s", $repository->unix_group);
  gitmanager_execute("sudo adduser %user %s", $repository->unix_group);
  gitmanager_execute("git init --bare --shared=group %s", $filepath);
  gitmanager_execute("sudo chgrp -R %s %s", $repository->unix_group, $filepath);

  // Record this new repository in the database so we may find 
  // it later to run tasks against it.
  $repository->filepath = $filepath;
  $repository->modified = time();
  drupal_write_record('gitmanager_repositories', $repository, 'rid');

  // Install the drush git hooks system so that the repo access
  // maybe verified by Drupal and changes in the repo can fire 
  // events in Drupal.
  $template = dirname(__FILE__) . '/gitmanager.hook_template.php';
  gitmanager_execute('drush githooks-install --template=%s -y %s', $template, $repository->filepath);

  gitmanager_provision('gitmanager_add_repo_user', $repository->rid, $repository->uid);
}

/**
 * Create a new Unix user.
 */
function gitmanager_create_unix_user($name, $uid) {
  // Check username doesn't already exist on the system.
  $users_info = file('/etc/passwd');

  try {
    foreach ($users_info as $line) {
      $user_info = explode(':', $line);
      if ($user_info[0] == $name) {
        // If the user's default shell isn't git-shell then its too dangerous
        // to allow the Drupal user be associated with it. Don't create the user.
        if (trim($user_info[6]) != '/usr/bin/git-shell') {
          throw new Exception("User's shell is insecure and cannot be used with Drupal.");
        }
        $unix_user = $user_info[0];
        break;
      }
    }

    // If the user doesn't exist. Then we'll want to create one.
    if (!isset($unix_user)) {
      if (!gitmanager_execute('sudo adduser --shell /usr/bin/git-shell --gecos "" --disabled-password --ingroup nogroup --no-create-home %s', $name)) {
        throw new Exception("Failed to create user account '$name'");
      }
    }
  }
  catch (Exception $e) {
    drush_set_error(__FUNCTION__, $e->getMessage());

    // User creation failed so dis-associate the user with the account.
    db_query("DELETE FROM {gitmanager_unix_users} WHERE uid = %d AND name = '%s'", $uid, $name);

    $message = 'messages|' . serialize(array('error' => array(t($e->getMessage()))));

    // Tell the logged in user if present.
    db_query("UPDATE {sessions} SET session = CONCAT(session, '%s') WHERE uid = %d", $message, $uid);
  }
}

/**
 * Update a users ssh-keys.
 */
function gitmanager_update_user_keys($uid) {
  $account = user_load($uid);
  if (!isset($account->unix_username)) {
    watchdog('gitmanager', __FUNCTION__ . ": User $uid doesn't have an associated unix account to update ssh-keys against", array(), WATCHDOG_ERROR);
    return;
  }
  $rs = db_query("SELECT ssh_key FROM {gitmanager_user_keys} WHERE uid = %d", $uid);
  $keys = array();
  while ($key = db_result($rs)) {
    $keys[] = $key;
  }
  $filename = variable_get('gitmanager_authorized_keys', '/gitmanager/authorized_keys') . '/' . $account->unix_username;
  if (empty($keys) && file_exists($filename)) {
    unlink($filename);
  }
  else {
    file_put_contents($filename, implode(PHP_EOL, $keys) . PHP_EOL);
  }
}

/**
 * Add a user to a repository's unix group to allow access.
 */
function gitmanager_add_repo_user($rid, $uid) {
  $repository = gitmanager_repository_load($rid);
  $account = user_load($uid);
  if (isset($account->unix_username, $repository->unix_group)) {
    if (!gitmanager_execute('sudo adduser %s %s', $account->unix_username, $repository->unix_group)) {
      drush_set_error(__FUNCTION__, sprintf("Unable to add user %s to group %s", $account->unix_username, $repository->unix_group));
    }
  }
}
