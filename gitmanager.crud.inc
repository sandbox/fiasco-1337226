<?php

/**
 * Create/Edit repository form.
 */
function gitmanager_repository_form($form_state, $repository = FALSE) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Repository name',
    '#description' => 'The name of the repository. That shouldn\'t contain a ".git" suffix. Name must be lower case and without spaces.',
    '#default_value' => $repository ? $repository->name : '',
    '#disabled' => (bool) $repository,
  );

  if ($repository) {
    $form['rid'] = array('#type' => 'hidden', '#value' => $repository->rid);
  }

  $form['op'][] = array('#type' => 'submit', '#value' => $repository ? 'update' : 'create');
  return $form;
}

/**
 * Validation: gitmanager_repository_form().
 */
function gitmanager_repository_form_validate($form, $form_state) {
  $name = $form_state['values']['name'];
 
  if (preg_match('/[^a-zA-Z0-9_]/',  $name)) {
    form_set_error('name', "Repository name contains illegal values");
  } 
}

/**
 * Submission: gitmanager_repository_form().
 */
function gitmanager_repository_form_submit(&$form, $form_state) {
  $name = $form_state['values']['name'];

  global $user;
  if (isset($form_state['values']['rid'])) {
    $repository = gitmanager_repository_load($form_state['values']['rid']);
  }
  else {
    $repository = new stdClass;
    $repository->filepath = $name;
    $repository->created = time();
    $repository->unix_group = $name;
  }
  $repository->name = $name;
  $repository->modified = time();
  $repository->uid = $user->uid;
  $repository->access = 1;

  $op = isset($repository->rid) ? 'update' : 'insert';
  drupal_alter('gitmanager_repository', $repository);

  if (!isset($repository->rid)) {
    drupal_write_record('gitmanager_repositories', $repository);
    drupal_write_record('gitmanager_access_repositories', $repository);
    gitmanager_provision('gitmanager_create_repository', array($repository));  
  }
  else {
    drupal_write_record('gitmanager_repositories', $repository, 'rid');
  }

  module_invoke_all('gitmanager_repository_' . $op, $repository);

  $form['#redirect'] = 'repository/' . $repository->rid;
}

/**
 * View Git Repository
 */
function gitmanager_repository_view($repository) {
  $content = array();

  // Generate instructions to clone git repository.
  global $user;
  $account = user_load($user->uid);
  if (!isset($account->unix_username)) {
    $content['clone_info']['#value'] = '<em>' . t('You do not yet have an account to access Git with. Please !link', array('!link' => l('create one', 'user/' . $user->uid . '/git-access'))) . '</em>';
  }
  else {
    $location = parse_url(variable_get('gitmanager_repositories_location', 'gitmanager@localhost/gitmanager'));
    $hostname = $location['host'] == 'localhost' ? shell_exec('hostname -f') : $location['host'];
    $content['clone_info'] = array(
      '#type' => 'textfield',
      '#title' => 'Git repository instructions',
      '#description' => 'Use this command to check out a clone of this repository',
      '#value' => 'git clone git+ssh://' . $account->unix_username . '@' . $hostname . $repository->filepath,
    );
  }


  $record = (array) $repository;
  unset($record['rid'], $record['unix_group'], $record['filepath']);
  $record['created'] = format_date($record['created']);
  $record['modified'] = format_date($record['modified']);

  foreach ($record as $key => $value) {
    $rows[] = array(array('data' => ucwords($key), 'header' => TRUE), $value);
  }
  $content['repository']['#value'] = theme('table', array(), $rows);
  $content['repository']['#weight'] = 10;

  return drupal_render($content);
}
