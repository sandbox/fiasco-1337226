<?php

/**
 * Implementation of hook_githooks_event_post_update().
 */
function gitmanager_log_githooks_event_post_update($branch_ref) {
  $filepath = drush_get_option('repository');
  // No point in continuing if we're not tracking this repository.
  if (!$rid = db_result(db_query("SELECT rid FROM {gitmanager_repositories} WHERE filepath = '%s'", $filepath))) {
    return;
  }
  $repository = gitmanager_repository_load($rid);

  // Determine if this update is pushing new refs to a branch or deleting it.
  $op = file_exists($repository->filepath . '/' . $branch_ref) ? 'update' : 'delete';
  gitmanager_provision('gitmanager_log_' . $op . '_ref', array($branch_ref, $repository));
}

/**
 * Helper function to deal with deleted branches.
 */
function gitmanager_log_delete_ref($ref, $repository) {
  $ref_info = explode('/', $ref);
  $branch = gitmanager_log_label_load(array('label' => $ref, 'type' => $ref_info[1], 'rid' => $repository->rid));
  // If the branch is being removed and it doesn't exist in our database then
  // there is nothing more for us to do.
  if (!$branch) {
    return;
  }
  db_query('DELETE FROM {gitmanager_repository_labels} WHERE lid = %d AND rid = %d', $branch->lid, $branch->rid);
  db_query('DELETE FROM {gitmanager_label_commits} WHERE lid = %d', $branch->lid);
  // Clean up commits log.
  db_query('DELETE FROM {gitmanager_commit_logs} WHERE cid NOT IN (SELECT cid FROM {gitmanager_label_commits})');
}

/**
 * Helper function to deal with updated branches.
 */
function gitmanager_log_update_ref($ref, $repository) {
  $ref_info = explode('/', $ref);
  $branch = gitmanager_log_label_load(array('label' => $ref, 'type' => $ref_info[1], 'rid' => $repository->rid));

  // Branch has newly been created. Add it to the database.
  // While we call this a branch, it could also be a tag or
  // other ref.
  if (!$branch) {
    $branch = new StdClass; 
    $branch->rid = $repository->rid;
    $branch->type = $ref_info[1];
    $branch->label = $ref;
    if (!drupal_write_record('gitmanager_repository_labels', $branch)) {
      watchdog('gitmanager_log', 'Failed to write branch to database', array(), WATCHDOG_ERROR);
      return;
    }
  }

  if ($branch->type = 'tags') {
    gitmanager_provision('gitmanager_log_save_revision', array($rev, $branch, $repository));
    return;
  }
  // Otherwise the assumption is that type is 'heads'.

  // Update branch with new commits.
  $branch_rev = trim(file_get_contents($repository->filepath . '/' . $branch->label));

  $known_rev = db_result(db_query("SELECT cl.commit FROM {gitmanager_commit_logs} cl INNER JOIN {gitmanager_label_commits} lc ON lc.cid = cl.cid WHERE lc.lid = %d ORDER BY cl.committer_date DESC LIMIT 1"));

  // If we've parsed in part of this branch then we don't need to do the whole
  // thing. Instead just get the revs that are not in the database yet.
  if ($known_rev) {
    $status = drush_shell_exec('git --git-dir %s rev-list %s..%s', $repository->filepath, $known_rev, $branch_rev);
  }
  // If the branch is new then we'll need to setup references on the fly.
  else {
    $status = drush_shell_exec('git --git-dir %s rev-list %s', $repository->filepath, $branch->label);
  }
  if (!$status) {
    watchdog('gitmanager', 'A error occured while retrieving Git logs. Branch wil lnot be able to be parsed into the database', array(), WATCHDOG_ERROR);
    return;
  }
  foreach (drush_shell_exec_output() as $rev) {
    $rev = trim($rev);
    gitmanager_provision('gitmanager_log_save_revision', array($rev, $branch, $repository));
  }
}

/**
 * Store a ref into the database.
 */
function gitmanager_log_save_revision($rev, $branch, $repository) {
  $ref_info_map = array(
    '%H' => 'commit',
    '%P' => 'parents',
    '%an' => 'author_name',
    '%ae' => 'author_email',
    '%at' => 'author_date',
    '%cn' => 'committer_name',
    '%ce' => 'committer_email',
    '%ct' => 'committer_date',
    '%s'  => 'subject',
  );

  $pretty = implode('%n', array_keys($ref_info_map)) . '%nENDOFOUTPUTGITMESSAGEHERE';
  if (!drush_shell_exec('git --git-dir %s show --numstat --summary --pretty=format:%s %s', $repository->filepath, $pretty, $rev)) {
    return FALSE;
  }

  $output = drush_shell_exec_output();

  $commit = array();
  foreach (array_values($ref_info_map) as $idx => $key) {
    $commit[$key] = iconv("UTF-8", "UTF-8//IGNORE", trim($output[$idx]));
    unset($output[$idx]);
  }
  while (($message = trim(array_shift($output))) && ($message != 'ENDOFOUTPUTGITMESSAGEHERE')) {
    $commit['subject'] .= PHP_EOL . $message;
  }
  $commit['parents'] = explode(' ', $commit['parents']);
  $commit['merge'] = count($commit['parents']) > 1;

  while ($line = array_shift($output)) {
    $op = array();
    // Parse file actions.
    if (preg_match('/^ (\S+) (\S+) (\S+) (.+)$/', $line, $matches)) {
      $filepath = $matches[4];
      switch ($matches[1]) {
        case 'create':
          $op['filepath'] = $filepath;
          $op['type'] = 'create';
          break;
        case 'delete':
          $op['filepath'] = $filepath;
          $op['type'] = 'delete';
          break;
      }

    }
    // Parse the diffstat for the changed files.
    elseif (preg_match('/^(\S+)' . "\t" . '(\S+)' . "\t" . '(.+)$/', $line, $matches)) {
      $filepath = $matches[3];
      $op['filepath'] = $filepath;
      $op['type'] = 'update';
    }
    if (!empty($op)) {
      $commit['operations'][] = $op;
    }
  }
  $commit['glid'] = sha1(serialize($commit));

  if (!$existing_commit = gitmanager_log_commit_load(array('commit' => $commit['commit'], 'glid' => $commit['glid']))) {
    if (!drupal_write_record('gitmanager_commit_logs', $commit)) {
      watchdog('gitmanager_log', 'Failed to add commit to {gitmanger_commit_logs}: %hash', array('%hash' => $commit['commit']), WATCHDOG_ERROR);
      return;
    }
    $existing_commit = (object) $commit;
  }
  $existing_commit->rid = $repository->rid;

  if (!db_result(db_query("SELECT 1 FROM {gitmanager_label_commits} WHERE cid = %d AND lid = %d", $existing_commit->cid, $branch->lid))) {
    $record = array('cid' => $existing_commit->cid, 'lid' => $branch->lid);
    drupal_write_record('gitmanager_label_commits', $record);
  }
}
