#!/usr/bin/env php
<?php
/**
 * This hook is managed by Drush to facilitate Drupal development in Git.
 */
$event = basename(__FILE__);
$repository = trim(shell_exec(sprintf("export GIT_DIR=%s; git rev-parse --git-dir", realpath(dirname(__FILE__) . '/..'))));

// Get the arguments passed by Git.
$arguments = $argv;
array_shift($arguments);

foreach ($arguments as &$arg) {
  $arg = escapeshellarg($arg);
}

$command = "drush githooks-event %root --repository=$repository $event " . implode(' ', $arguments);

passthru($command, $status);
exit((int) ($status == 1));

